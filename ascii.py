def toUppercase(letter):
    a = ord(letter) - 32
    return chr(a)

def toLowercase(letter):
    a = ord(letter) + 32
    return chr(a)

def isAlphabet(char):
    if(65 <= ord(char) <= 90):
        return True
    elif(97 <= ord(char) <= 122):
        return True
    return False

def isDigit(char):
    if(48 <= ord(char) <= 57):
        return True
    return False

def isSpecial(char):
    if(isAlphabet(char) == False and isDigit(char) == False):
        if not(0 <= ord(char) <= 31):
            return True
    return False

def main():
    print("should be A: " + toUppercase("a"))
    print("should be a: " + toLowercase("A"))
    print("should be true: " + str(isAlphabet("Z")))
    print("should be false: " + str(isAlphabet("1")))
    print("should be true: " + str(isDigit("1")))
    print("should be false: " + str(isDigit("a")))
    print("should be true: " + str(isSpecial("%")))
    print("should be false: " + str(isSpecial("h")))
