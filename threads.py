import threading
import time

def countUpToTen():
    for i in range(11):
        print(i)                    #print the number
        time.sleep(1)               #sleep 1 second in each loop iteration
        
def countDownToZero():
    for i in reversed(range(11)):   #reverses order
        print(i)
        time.sleep(1)

def main():
    thread1 = threading.Thread(target= countUpToTen)
    thread2 = threading.Thread(target= countDownToZero)

    #start = time.perf_counter()
    
    thread1.start()                 #launching / starting threads
    thread2.start()

    thread1.join()
    thread2.join()
    
    #done = time.perf_counter()
    #print(f'Finished in {round(done-start, 2)} seconds')

main()